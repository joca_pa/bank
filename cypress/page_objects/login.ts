///<reference types="cypress"/>

import {Given, When, Then} from 'cypress-cucumber-preprocessor/steps';

export class LoginPage{

    private static instance : LoginPage

    private constructor(){ }

    public static getInstance(): LoginPage {
        if(!LoginPage.instance)
            LoginPage.instance = new LoginPage()
        return LoginPage.instance
    }
    
    public go = () : LoginPage => {
        cy.visit('https://parabank.parasoft.com/parabank/index.htm')
        return LoginPage.getInstance()
    }
}