///<reference types="cypress" />

import { LoginPage } from '../page_objects/login'

const loginPage = LoginPage.getInstance()

export const goToPage = () =>{
    loginPage.go()
}